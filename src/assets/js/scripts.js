window.app = {};

(function(window, document, $) {
  "use strict";

  window.app = window.app || {};

  app.init = function() {
    
    this.initVue = function() {
      var vm;
      
      Vue.component('app-menu', {
        props: {
          openmenu: {
            type: Boolean,
            required: true,
            default: false
          }
        },
        template: `
        <aside class="widget main-menu-container" :class="{'opened' : openmenu }">
          <div class="inner-wrapper">

            <div class="btn-container">
              <span role="button"  @click.prevent="initHideMenu" class="close-menu">X</span>
            </div>  

            <div class="reclaim"> 
              <p>
                {{ message }}
              </p>
            </div>

            <nav class="main-menu">
              <a href="#">about</a>
              <a href="#">how it works</a>
              <a href="#">contact</a>
            </nav>

          </div>
        </aside>     
        `,        
        data: function() {
          return {
            message: 'Your best weather app',
            counter: 0
          }
        },
        methods: {
          initHideMenu: function() {
            this.$emit('close');
          }
        },
        computed: {
          
        }
       
      });

      return vm = new Vue({
        el: '#app',
        data: function() {
          return {
            menuProp: false,
            infoWeather: false,
            cityName: '',
            countryCode: 'GR',
            weatherData: []
          }
        },
        methods: {
          initOpenMenu: function() {
            var _t1 = document.querySelector('.reclaim');
            var _t2 = document.querySelector('.main-menu');

            this.menuProp = !this.menuProp;

            TweenLite.to(_t1, 1, {opacity: 1, ease:Strong.easeInOut, delay: .4});
            TweenLite.to(_t2, 1, {opacity: 1, ease:Strong.easeInOut, delay: .8});
            
          },
          initAxios: function() {

            if(this.$refs.cityName.value === "") {
              return;
            }
            
            var _app = this;
            var _getRef = this.$refs.cityName.value;
            var _cleanValue = _getRef.split(',')[0];
            this.cityName = _cleanValue;
            var _url = 'http://api.openweathermap.org/data/2.5/forecast/daily?q='+ _app.cityName + ','+ _app.countryCode +'&units=metric&cnt=6&APPID=9ebf87d8944599d92120cfefd976cd20';

            axios.get(_url)
                 .then(function(response) {
                   console.log(response);
                   // set the array
                   _app.weatherData = response.data.list;
                 })
                 .catch(function(err) {
                   console.log('There is an error: ', err)
                 });


            //show the result page
            _app.infoWeather = !_app.infoWeather;
          
          },
          initConvertDays: function(t) {

            var days = [ "Sunday", "Monday", "Tuesday",
              "Wednesday", "Thursday", "Friday", "Saturday" ];

            var a = new Date(t * 1000);
            var date = a.getDay();
            return days[date];

          },
          initGetDate: function(t) {
            var theDate = new Date(t * 1000).toGMTString().split(" ");

            return theDate[2]+'. '+ theDate[1];
            
          },
          initWeatherIcons: function(w) {

            
            if(w === "Clear" || w === "Extreme") {
              return '<i class="flaticon-sun"></i>';
            }
            else if(w === "Rain") {
              return '<i class="flaticon-rain-cloud"></i>';
            }
            else if(w === "Snow") {
              return '<i class="flaticon-hail"></i>';
            }
            else if( w === "Clouds") {
              return '<i class="flaticon-summer"></i>';
            }
          }
        },
        watch: {
        }
      });

    }();

    this.initAutoComplete = function(){
      var _input = document.getElementById('location-input');
      var _options = {
        types: ['(cities)'],
        componentRestrictions: {country: "GR"}
      };
      new google.maps.places.Autocomplete( _input , _options );
    }();

  };

  window.addEventListener('load', function() {
    app.init();
  });

}(window, document, jQuery, undefined))