var path = require('path');

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';\n',
      },
      js: {
        src: [
          'bower_components/vue/dist/vue.min.js',
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/axios/dist/axios.min.js',
          'bower_components/gsap/src/minified/TweenMax.min.js',
          'bower_components/gsap/src/minified/plugins/CSSPlugin.min.js',
          'src/assets/js/scripts.js'
        ],
        dest: 'src/assets/js/_bundle.js',
      }
    },
    copy: {
      main: {
        expand: true,
        cwd: 'src/',
        src: '**',
        dest: 'public/'
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['concat:js' , 'copy']);

}